%skip   space          \s

// Scalars.
%token  true           true
%token  false          false
%token  null           null
// Strings.
%token  quote_         "        -> string
%token  string:string  [^"]+
%token  string:_quote  "        -> default
// Ranges.
%token  bracket_       \[
%token  _bracket       \]
%token  brace_         {
%token  _brace         }
%token  range_inf      \*
%token  range_sep      TO

// In conditions.
%token  in_open        \(
%token  in_close       \)
// Rest.
%token  hash           #
%token  colon          :
%token  comma          ,
%token  dot            \.
%token  hyphen         -
%token  at             @
%token  exists         ∃|∄


%token  or             or|OR|\|\|
%token  and            and|AND|&&
%token  comparison     =|((>|<)=?)

%token  date           (\d{4})-(0\d|10|11|12)-((0|1|2)\d|30|31)
%token  time_separator T
%token  time           ((0|1)\d|20|21|22|23|24):(0|1|2|3|4|5)\d(:(0|1|2|3|4|5)\d)?

%token  number         \d+\.?\d*
%token  word           \w+

b:
    conjunction() | expression()

#conjunction:
    expression() ((<or>|<and>)? expression())+

expression:
    scalar() | negation() | field() | hash() | subquery()

scalar:
    <true> | <false> | <null> | number() | string() | date() | time() | datetime()

number:
    <number>

string:
    ::quote_:: <string> ::_quote:: | <word>

date:
    <date>

time:
    <time>

#range:
    ::bracket_:: range_value() ::range_sep:: range_value() ::_bracket::
    | ::brace_:: range_value() ::range_sep:: range_value() ::_brace::

range_value:
    <range_inf> | number() | date() | time() | datetime()

#in:
    ::in_open:: (scalar())+ ::in_close::

#subquery:
    ::in_open:: conjunction() ::in_close::

#datetime:
    date() ::time_separator:: time()

#negation:
    ::hyphen:: (scalar()|field())

#hash:
    ::hash:: <word>

#field_name:
    <word> (::dot:: <word>)?

#field:
    field_name() ::colon:: <comparison>? (<exists>|scalar()|range()|in())

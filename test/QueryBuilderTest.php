<?php

namespace test;

use Hoa\Compiler\Exception\UnrecognizedToken;
use Hoa\Compiler\Llk\TreeNode;
use Hoa\Compiler\Visitor\Dump;
use query_builder\QueryBuilder;
use PHPUnit\Framework\TestCase;

class QueryBuilderTest extends TestCase
{
    const EXISTS     = '∃';
    const NOT_EXISTS = '∄';

    public function simpleQueries()
    {
        return [
            ['query' => 'test', 'expected' => ">  token(word, test)\n"],
            ['query' => 1, 'expected' => ">  token(number, 1)\n"],
            ['query' => '555555555555', 'expected' => ">  token(number, 555555555555)\n"],
            // ['query' => '-9876543210', 'expected' => ">  token(number, -9876543210)\n"],
            ['query' => '1234.23', 'expected' => ">  token(number, 1234.23)\n"],
            // ['query' => '-1234.23', 'expected' => ">  token(number, -1234.23)\n"],
            ['query' => '2018-01-01', 'expected' => ">  token(date, 2018-01-01)\n"],
            ['query' => '2018-12-31', 'expected' => ">  token(date, 2018-12-31)\n"],
            ['query' => '12:00', 'expected' => ">  token(time, 12:00)\n"],
            ['query' => '12:34:56', 'expected' => ">  token(time, 12:34:56)\n"],
            [
                'query'    => '2018-01-10T12:00',
                'expected' => ">  #datetime\n>  >  token(date, 2018-01-10)\n>  >  token(time, 12:00)\n",
            ],
            // ['query' => '2018-01-10 12:00', 'expected' => "\n"],
            ['query' => '"2018-01-10 12:00"', 'expected' => ">  token(string:string, 2018-01-10 12:00)\n"],
            // ['query' => '@abc', 'expected' => "\n"],
            // ['query' => '💩', 'expected' => "\n"],
            // ['query' => ':)', 'expected' => "\n"],
            // ['query' => 'http://netis.pl', 'expected' => "\n"],
        ];
    }

    public function simpleNegations()
    {
        return [
            ['query' => '-abc', 'expected' => ">  #negation\n>  >  token(word, abc)\n"],
        ];
    }

    public function simpleConditions()
    {
        return [
            [
                'query'    => 'test || "alfa beta"',
                'expected' => ">  #conjunction\n>  >  token(word, test)\n>  >  token(or, ||)\n>  >  token(string:string, alfa beta)\n",
            ],
            [
                'query'    => 'test or "alfa beta"',
                'expected' => ">  #conjunction\n>  >  token(word, test)\n>  >  token(or, or)\n>  >  token(string:string, alfa beta)\n",
            ],
            [
                'query'    => 'test OR "alfa beta"',
                'expected' => ">  #conjunction\n>  >  token(word, test)\n>  >  token(or, OR)\n>  >  token(string:string, alfa beta)\n",
            ],
            [
                'query'    => 'test || 2',
                'expected' => ">  #conjunction\n>  >  token(word, test)\n>  >  token(or, ||)\n>  >  token(number, 2)\n",
            ],
            [
                'query'    => 'test || "alfa beta" || gamma',
                'expected' => ">  #conjunction\n>  >  token(word, test)\n>  >  token(or, ||)\n>  >  token(string:string, alfa beta)\n>  >  token(or, ||)\n>  >  token(word, gamma)\n",
            ],

            [
                'query'    => 'test && "alfa beta"',
                'expected' => ">  #conjunction\n>  >  token(word, test)\n>  >  token(and, &&)\n>  >  token(string:string, alfa beta)\n",
            ],
            [
                'query'    => 'test and "alfa beta"',
                'expected' => ">  #conjunction\n>  >  token(word, test)\n>  >  token(and, and)\n>  >  token(string:string, alfa beta)\n",
            ],
            [
                'query'    => 'test AND "alfa beta"',
                'expected' => ">  #conjunction\n>  >  token(word, test)\n>  >  token(and, AND)\n>  >  token(string:string, alfa beta)\n",
            ],
            [
                'query'    => 'test && 2',
                'expected' => ">  #conjunction\n>  >  token(word, test)\n>  >  token(and, &&)\n>  >  token(number, 2)\n",
            ],
            [
                'query'    => 'test && "alfa beta" && gamma',
                'expected' => ">  #conjunction\n>  >  token(word, test)\n>  >  token(and, &&)\n>  >  token(string:string, alfa beta)\n>  >  token(and, &&)\n>  >  token(word, gamma)\n",
            ],

            [
                'query'    => 'test "alfa beta"',
                'expected' => ">  #conjunction\n>  >  token(word, test)\n>  >  token(string:string, alfa beta)\n",
            ],
            ['query' => 'test 2', 'expected' => ">  #conjunction\n>  >  token(word, test)\n>  >  token(number, 2)\n"],
            [
                'query'    => 'test "alfa beta" gamma',
                'expected' => ">  #conjunction\n>  >  token(word, test)\n>  >  token(string:string, alfa beta)\n>  >  token(word, gamma)\n",
            ],
        ];
    }

    public function fieldQueries()
    {
        return [
            [
                'query'    => 'last_name:test',
                'expected' => ">  #field\n>  >  #field_name\n>  >  >  token(word, last_name)\n>  >  token(word, test)\n",
            ],
            [
                'query'    => 'description:test',
                'expected' => ">  #field\n>  >  #field_name\n>  >  >  token(word, description)\n>  >  token(word, test)\n",
            ],
            [
                'query'    => 'last_name:00008',
                'expected' => ">  #field\n>  >  #field_name\n>  >  >  token(word, last_name)\n>  >  token(number, 00008)\n",
            ],
            [
                'query'    => 'last_name:"00008"',
                'expected' => ">  #field\n>  >  #field_name\n>  >  >  token(word, last_name)\n>  >  token(string:string, 00008)\n",
            ],
            [
                'query'    => 'last_name:"test abc"',
                'expected' => ">  #field\n>  >  #field_name\n>  >  >  token(word, last_name)\n>  >  token(string:string, test abc)\n",
            ],
            [
                'query'    => 'status:=1234',
                'expected' => ">  #field\n>  >  #field_name\n>  >  >  token(word, status)\n>  >  token(comparison, =)\n>  >  token(number, 1234)\n",
            ],
            [
                'query'    => 'status:>1234',
                'expected' => ">  #field\n>  >  #field_name\n>  >  >  token(word, status)\n>  >  token(comparison, >)\n>  >  token(number, 1234)\n",
            ],
            [
                'query'    => 'status:<1234',
                'expected' => ">  #field\n>  >  #field_name\n>  >  >  token(word, status)\n>  >  token(comparison, <)\n>  >  token(number, 1234)\n",
            ],
            [
                'query'    => 'status:>=1234',
                'expected' => ">  #field\n>  >  #field_name\n>  >  >  token(word, status)\n>  >  token(comparison, >=)\n>  >  token(number, 1234)\n",
            ],
            [
                'query'    => 'status:<=1234',
                'expected' => ">  #field\n>  >  #field_name\n>  >  >  token(word, status)\n>  >  token(comparison, <=)\n>  >  token(number, 1234)\n",
            ],
            [
                'query'    => 'visible_in_contacts:true',
                'expected' => ">  #field\n>  >  #field_name\n>  >  >  token(word, visible_in_contacts)\n>  >  token(true, true)\n",
            ],
            [
                'query'    => 'visible_in_contacts:false',
                'expected' => ">  #field\n>  >  #field_name\n>  >  >  token(word, visible_in_contacts)\n>  >  token(false, false)\n",
            ],
            // ['query' => 'inet:1.2.3.4', 'expected' => ">  #field\n>  >  token(word, last_name)\n>  >  token(word, test)\n"],
            // ['query' => '-inet:1.2.3.4', 'expected' => ">  #field\n>  >  token(word, last_name)\n>  >  token(word, test)\n"],
        ];
    }

    public function fieldConditions()
    {
        return [
            [
                'query'    => 'last_name:test || last_name:system',
                'expected' => <<<TXT
>  #conjunction
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, last_name)
>  >  >  token(word, test)
>  >  token(or, ||)
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, last_name)
>  >  >  token(word, system)

TXT,
            ],
            [
                'query'    => 'last_name:test or last_name:system',
                'expected' => <<<TXT
>  #conjunction
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, last_name)
>  >  >  token(word, test)
>  >  token(or, or)
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, last_name)
>  >  >  token(word, system)

TXT,
            ],
            [
                'query'    => 'last_name:test OR last_name:system',
                'expected' => <<<TXT
>  #conjunction
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, last_name)
>  >  >  token(word, test)
>  >  token(or, OR)
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, last_name)
>  >  >  token(word, system)

TXT,
            ],
            [
                'query'    => 'last_name:test && last_name:system',
                'expected' => <<<TXT
>  #conjunction
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, last_name)
>  >  >  token(word, test)
>  >  token(and, &&)
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, last_name)
>  >  >  token(word, system)

TXT,
            ],
            [
                'query'    => 'last_name:test and last_name:system',
                'expected' => <<<TXT
>  #conjunction
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, last_name)
>  >  >  token(word, test)
>  >  token(and, and)
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, last_name)
>  >  >  token(word, system)

TXT,
            ],
            [
                'query'    => 'last_name:test AND last_name:system',
                'expected' => <<<TXT
>  #conjunction
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, last_name)
>  >  >  token(word, test)
>  >  token(and, AND)
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, last_name)
>  >  >  token(word, system)

TXT,
            ],
            [
                'query'    => 'last_name:test last_name:system',
                'expected' => <<<TXT
>  #conjunction
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, last_name)
>  >  >  token(word, test)
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, last_name)
>  >  >  token(word, system)

TXT,
            ],
            [
                'query'    => 'last_name:test -status:1',
                'expected' => <<<TXT
>  #conjunction
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, last_name)
>  >  >  token(word, test)
>  >  #negation
>  >  >  #field
>  >  >  >  #field_name
>  >  >  >  >  token(word, status)
>  >  >  >  token(number, 1)

TXT,
            ],
            [
                'query'    => 'last_name:"test abc" OR last_name:"alfa beta"',
                'expected' => <<<TXT
>  #conjunction
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, last_name)
>  >  >  token(string:string, test abc)
>  >  token(or, OR)
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, last_name)
>  >  >  token(string:string, alfa beta)

TXT,
            ],
            [
                'query'    => 'visible_in_contacts:true || visible_in_contacts:false',
                'expected' => <<<TXT
>  #conjunction
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, visible_in_contacts)
>  >  >  token(true, true)
>  >  token(or, ||)
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, visible_in_contacts)
>  >  >  token(false, false)

TXT,
            ],
        ];
    }

    public function fieldNegations()
    {
        return [
            [
                'query'    => '-visible_in_contacts:false',
                'expected' => <<<TXT
>  #negation
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, visible_in_contacts)
>  >  >  token(false, false)

TXT,
            ],
            [
                'query'    => '-status:5',
                'expected' => <<<TXT
>  #negation
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, status)
>  >  >  token(number, 5)

TXT,
            ],
            [
                'query'    => '-created_at:2018-01-01',
                'expected' => <<<TXT
>  #negation
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, created_at)
>  >  >  token(date, 2018-01-01)

TXT,
            ],
            [
                'query'    => '-last_name:test',
                'expected' => <<<TXT
>  #negation
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, last_name)
>  >  >  token(word, test)

TXT,
            ],
        ];
    }

    public function fieldRanges()
    {
        return [
            [
                'query'    => '-created_at:[2018-01-01 TO 2018-10-30]',
                'expected' => <<<TXT
>  #negation
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, created_at)
>  >  >  #range
>  >  >  >  token(date, 2018-01-01)
>  >  >  >  token(date, 2018-10-30)

TXT
,
            ],
            [
                'query'    => 'created_at:[2018-01-01 TO 2018-10-30]',
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, created_at)
>  >  #range
>  >  >  token(date, 2018-01-01)
>  >  >  token(date, 2018-10-30)

TXT
,
            ],
            [
                'query'    => 'created_at:{2018-01-01 TO 2018-10-30}',
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, created_at)
>  >  #range
>  >  >  token(date, 2018-01-01)
>  >  >  token(date, 2018-10-30)

TXT
,
            ],
            [
                'query'    => 'created_at:[2018-01-01 TO 2018-10-30]',
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, created_at)
>  >  #range
>  >  >  token(date, 2018-01-01)
>  >  >  token(date, 2018-10-30)

TXT
,
            ],
            [
                'query'    => 'created_at:[2018-01-01 TO 2018-10-30]',
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, created_at)
>  >  #range
>  >  >  token(date, 2018-01-01)
>  >  >  token(date, 2018-10-30)

TXT
,
            ],
            [
                'query'    => 'created_at:[* TO 2018-10-30]',
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, created_at)
>  >  #range
>  >  >  token(range_inf, *)
>  >  >  token(date, 2018-10-30)

TXT
,
            ],
            [
                'query'    => 'created_at:[2018-01-01 TO *]',
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, created_at)
>  >  #range
>  >  >  token(date, 2018-01-01)
>  >  >  token(range_inf, *)

TXT
,
            ],

            [
                'query'    => 'created_at:[12:00 TO 15:00]',
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, created_at)
>  >  #range
>  >  >  token(time, 12:00)
>  >  >  token(time, 15:00)

TXT,
            ],
            [
                'query'    => 'created_at:[* TO 15:00]',
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, created_at)
>  >  #range
>  >  >  token(range_inf, *)
>  >  >  token(time, 15:00)

TXT,
            ],
            [
                'query'    => 'created_at:[12:00 TO *]',
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, created_at)
>  >  #range
>  >  >  token(time, 12:00)
>  >  >  token(range_inf, *)

TXT,
            ],

            [
                'query'    => 'status:[0 TO 5]',
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, status)
>  >  #range
>  >  >  token(number, 0)
>  >  >  token(number, 5)

TXT,
            ],
            [
                'query'    => 'status:{0 TO 5}',
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, status)
>  >  #range
>  >  >  token(number, 0)
>  >  >  token(number, 5)

TXT,
            ],
            [
                'query'    => 'status:[* TO 5]',
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, status)
>  >  #range
>  >  >  token(range_inf, *)
>  >  >  token(number, 5)

TXT,
            ],
            [
                'query'    => 'status:{0 TO *}',
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, status)
>  >  #range
>  >  >  token(number, 0)
>  >  >  token(range_inf, *)

TXT,
            ],
        ];
    }

    public function fieldInConditions()
    {
        return [
            [
                'query'    => 'last_name:(system)',
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, last_name)
>  >  #in
>  >  >  token(word, system)

TXT,
            ],
            [
                'query'    => 'last_name:(system aaa)',
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, last_name)
>  >  #in
>  >  >  token(word, system)
>  >  >  token(word, aaa)

TXT,
            ],
            [
                'query'    => 'last_name:(system aaa bbb)',
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, last_name)
>  >  #in
>  >  >  token(word, system)
>  >  >  token(word, aaa)
>  >  >  token(word, bbb)

TXT,
            ],
            [
                'query'    => 'last_name:(system aaa bbb ccc)',
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, last_name)
>  >  #in
>  >  >  token(word, system)
>  >  >  token(word, aaa)
>  >  >  token(word, bbb)
>  >  >  token(word, ccc)

TXT,
            ],
            [
                'query'    => 'status:(1 2 3)',
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, status)
>  >  #in
>  >  >  token(number, 1)
>  >  >  token(number, 2)
>  >  >  token(number, 3)

TXT,
            ],
            [
                'query'    => '-last_name:(system aaa bbb)',
                'expected' => <<<TXT
>  #negation
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, last_name)
>  >  >  #in
>  >  >  >  token(word, system)
>  >  >  >  token(word, aaa)
>  >  >  >  token(word, bbb)

TXT,
            ],
            [
                'query'    => '-status:(1 2 3)',
                'expected' => <<<TXT
>  #negation
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, status)
>  >  >  #in
>  >  >  >  token(number, 1)
>  >  >  >  token(number, 2)
>  >  >  >  token(number, 3)

TXT,
            ],
            [
                'query'    => 'last_name:(system aaa bbb "special user")',
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, last_name)
>  >  #in
>  >  >  token(word, system)
>  >  >  token(word, aaa)
>  >  >  token(word, bbb)
>  >  >  token(string:string, special user)

TXT,
            ],
            [
                'query'    => '-status:(negative_returned positive_returned closed_payout)',
                'expected' => <<<TXT
>  #negation
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, status)
>  >  >  #in
>  >  >  >  token(word, negative_returned)
>  >  >  >  token(word, positive_returned)
>  >  >  >  token(word, closed_payout)

TXT,
            ],
        ];
    }

    public function subFieldQueries()
    {
        return [
            [
                'query'    => 'client.name:"test"',
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, client)
>  >  >  token(word, name)
>  >  token(string:string, test)

TXT,
            ],
            [
                'query'    => 'client.id:1',
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, client)
>  >  >  token(word, id)
>  >  token(number, 1)

TXT,
            ],
            [
                'query'    => 'client.id:[1 TO 10]',
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, client)
>  >  >  token(word, id)
>  >  #range
>  >  >  token(number, 1)
>  >  >  token(number, 10)

TXT,
            ],
            [
                'query'    => 'client.name:"test" AND id:10',
                'expected' => <<<TXT
>  #conjunction
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, client)
>  >  >  >  token(word, name)
>  >  >  token(string:string, test)
>  >  token(and, AND)
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, id)
>  >  >  token(number, 10)

TXT,
            ],
            [
                'query'    => 'facilities.id:1',
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, facilities)
>  >  >  token(word, id)
>  >  token(number, 1)

TXT,
            ],
            [
                'query'    => 'facilities.name:"test"',
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, facilities)
>  >  >  token(word, name)
>  >  token(string:string, test)

TXT,
            ],
            [
                'query'    => 'facilities.id:[1 TO 10]',
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, facilities)
>  >  >  token(word, id)
>  >  #range
>  >  >  token(number, 1)
>  >  >  token(number, 10)

TXT,
            ],
            [
                'query'    => 'facilities.id:1 AND id:1',
                'expected' => <<<TXT
>  #conjunction
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, facilities)
>  >  >  >  token(word, id)
>  >  >  token(number, 1)
>  >  token(and, AND)
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, id)
>  >  >  token(number, 1)

TXT,
            ],
        ];
    }

    public function hashQueries()
    {
        return [
            [
                'query'    => '#visible',
                'expected' => <<<TXT
>  #hash
>  >  token(word, visible)

TXT,
            ],
            [
                'query'    => '#test_anonymous',
                'expected' => <<<TXT
>  #hash
>  >  token(word, test_anonymous)

TXT,
            ],
            [
                'query'    => '#visible #test_class',
                'expected' => <<<TXT
>  #conjunction
>  >  #hash
>  >  >  token(word, visible)
>  >  #hash
>  >  >  token(word, test_class)

TXT,
            ],
        ];
    }

    public function existsQueries()
    {
        return [
            [
                'query'    => 'status:' . self::EXISTS,
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, status)
>  >  token(exists, ∃)

TXT,
            ],
            [
                'query'    => 'status:' . self::NOT_EXISTS,
                'expected' => <<<TXT
>  #field
>  >  #field_name
>  >  >  token(word, status)
>  >  token(exists, ∄)

TXT,
            ],
        ];
    }

    public function complexQueries()
    {
        return [
            [
                'query'    => 'last_name:system AND (status:(1 2 3) OR value:[0 TO 100])',
                'expected' => <<<TXT
>  #conjunction
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, last_name)
>  >  >  token(word, system)
>  >  token(and, AND)
>  >  #subquery
>  >  >  #conjunction
>  >  >  >  #field
>  >  >  >  >  #field_name
>  >  >  >  >  >  token(word, status)
>  >  >  >  >  #in
>  >  >  >  >  >  token(number, 1)
>  >  >  >  >  >  token(number, 2)
>  >  >  >  >  >  token(number, 3)
>  >  >  >  token(or, OR)
>  >  >  >  #field
>  >  >  >  >  #field_name
>  >  >  >  >  >  token(word, value)
>  >  >  >  >  #range
>  >  >  >  >  >  token(number, 0)
>  >  >  >  >  >  token(number, 100)

TXT,
            ],
            [
                'query'    => '(last_name:system AND status:(1 2 3)) OR value:[0 TO 100]',
                'expected' => <<<TXT
>  #conjunction
>  >  #subquery
>  >  >  #conjunction
>  >  >  >  #field
>  >  >  >  >  #field_name
>  >  >  >  >  >  token(word, last_name)
>  >  >  >  >  token(word, system)
>  >  >  >  token(and, AND)
>  >  >  >  #field
>  >  >  >  >  #field_name
>  >  >  >  >  >  token(word, status)
>  >  >  >  >  #in
>  >  >  >  >  >  token(number, 1)
>  >  >  >  >  >  token(number, 2)
>  >  >  >  >  >  token(number, 3)
>  >  token(or, OR)
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, value)
>  >  >  #range
>  >  >  >  token(number, 0)
>  >  >  >  token(number, 100)

TXT,
            ],
            [
                'query'    => 'last_name:system AND status:(1 2 3) && value:[0 TO 100]',
                'expected' => <<<TXT
>  #conjunction
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, last_name)
>  >  >  token(word, system)
>  >  token(and, AND)
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, status)
>  >  >  #in
>  >  >  >  token(number, 1)
>  >  >  >  token(number, 2)
>  >  >  >  token(number, 3)
>  >  token(and, &&)
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, value)
>  >  >  #range
>  >  >  >  token(number, 0)
>  >  >  >  token(number, 100)

TXT,
            ],
            [
                'query'    => 'last_name:system OR status:(1 2 3) || value:[0 TO 100]',
                'expected' => <<<TXT
>  #conjunction
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, last_name)
>  >  >  token(word, system)
>  >  token(or, OR)
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, status)
>  >  >  #in
>  >  >  >  token(number, 1)
>  >  >  >  token(number, 2)
>  >  >  >  token(number, 3)
>  >  token(or, ||)
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, value)
>  >  >  #range
>  >  >  >  token(number, 0)
>  >  >  >  token(number, 100)

TXT,
            ],
            [
                'query'    => 'last_name:system AND status:(1 2 3) || value:[0 TO 100]',
                'expected' => <<<TXT
>  #conjunction
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, last_name)
>  >  >  token(word, system)
>  >  token(and, AND)
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, status)
>  >  >  #in
>  >  >  >  token(number, 1)
>  >  >  >  token(number, 2)
>  >  >  >  token(number, 3)
>  >  token(or, ||)
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, value)
>  >  >  #range
>  >  >  >  token(number, 0)
>  >  >  >  token(number, 100)

TXT,
            ],
            [
                'query'    => 'last_name:system AND status:(1 2 3) OR value:[0 TO 100]',
                'expected' => <<<TXT
>  #conjunction
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, last_name)
>  >  >  token(word, system)
>  >  token(and, AND)
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, status)
>  >  >  #in
>  >  >  >  token(number, 1)
>  >  >  >  token(number, 2)
>  >  >  >  token(number, 3)
>  >  token(or, OR)
>  >  #field
>  >  >  #field_name
>  >  >  >  token(word, value)
>  >  >  #range
>  >  >  >  token(number, 0)
>  >  >  >  token(number, 100)

TXT,
            ],
        ];
    }

    public function invalidQueries()
    {
        return [
            [
                'query'    => 'status:',
                'expected' => <<<TXT
Unexpected token "EOF" (EOF) at line 1 and column 8:
status:
       ↑
TXT,
            ],
            [
                'query'    => 'status:()',
                'expected' => <<<TXT
Unexpected token ")" (in_close) at line 1 and column 9:
status:()
        ↑
TXT,
            ],
            [
                'query'    => 'status:[0 TO ]',
                'expected' => <<<TXT
Unexpected token "]" (_bracket) at line 1 and column 14:
status:[0 TO ]
             ↑
TXT,
            ],
            [
                'query'    => 'status:[ TO 10]',
                'expected' => <<<TXT
Unexpected token "TO" (range_sep) at line 1 and column 10:
status:[ TO 10]
         ↑
TXT,
            ],
            [
                'query'    => 'status:[0 TO 10}',
                'expected' => <<<TXT
Unexpected token "}" (_brace) at line 1 and column 16:
status:[0 TO 10}
               ↑
TXT,
            ],
            [
                'query'    => 'status:{0 TO 10]',
                'expected' => <<<TXT
Unexpected token "]" (_bracket) at line 1 and column 16:
status:{0 TO 10]
               ↑
TXT,
            ],
            [
                'query'    => '12:',
                'expected' => <<<TXT
Unexpected token ":" (colon) at line 1 and column 3:
12:
  ↑
TXT,
            ],
            [
                'query'    => '12:2',
                'expected' => <<<TXT
Unexpected token ":" (colon) at line 1 and column 3:
12:2
  ↑
TXT,
            ],
            [
                'query'    => '12:23:',
                'expected' => <<<TXT
Unexpected token ":" (colon) at line 1 and column 6:
12:23:
     ↑
TXT,
            ],
            [
                'query'    => '12:23:3',
                'expected' => <<<TXT
Unexpected token ":" (colon) at line 1 and column 6:
12:23:3
     ↑
TXT,
            ],
        ];
    }

    private function parse($query, $expected)
    {
        $qb  = new QueryBuilder();
        $ast = $qb->parse($query);

        $dump = new Dump();

        $this->assertInstanceOf(TreeNode::class, $ast);
        $this->assertEquals($expected, $dump->visit($ast));
    }

    /**
     * @param string $query
     * @param string $expected
     *
     * @dataProvider simpleQueries
     */
    public function testSimpleQueries($query, $expected)
    {
        $this->parse($query, $expected);
    }

    /**
     * @param string $query
     * @param string $expected
     *
     * @dataProvider simpleNegations
     */
    public function testSimpleNegations($query, $expected)
    {
        $this->parse($query, $expected);
    }

    /**
     * @param string $query
     * @param string $expected
     *
     * @dataProvider simpleConditions
     */
    public function testSimpleConditions($query, $expected)
    {
        $this->parse($query, $expected);
    }

    /**
     * @param string $query
     * @param string $expected
     *
     * @dataProvider fieldQueries
     */
    public function testFieldQueries($query, $expected)
    {
        $this->parse($query, $expected);
    }

    /**
     * @param string $query
     * @param string $expected
     *
     * @dataProvider fieldConditions
     */
    public function testFieldConditions($query, $expected)
    {
        $this->parse($query, $expected);
    }

    /**
     * @param string $query
     * @param string $expected
     *
     * @dataProvider fieldNegations
     */
    public function testFieldNegations($query, $expected)
    {
        $this->parse($query, $expected);
    }

    /**
     * @param string $query
     * @param string $expected
     *
     * @dataProvider fieldRanges
     */
    public function testFieldRanges($query, $expected)
    {
        $this->parse($query, $expected);
    }

    /**
     * @param string $query
     * @param string $expected
     *
     * @dataProvider fieldInConditions
     */
    public function testFieldInConditions($query, $expected)
    {
        $this->parse($query, $expected);
    }

    /**
     * @param string $query
     * @param string $expected
     *
     * @dataProvider subFieldQueries
     */
    public function testSubFieldQueries($query, $expected)
    {
        $this->parse($query, $expected);
    }

    /**
     * @param string $query
     * @param string $expected
     *
     * @dataProvider hashQueries
     */
    public function testHashQueries($query, $expected)
    {
        $this->parse($query, $expected);
    }

    /**
     * @param string $query
     * @param string $expected
     *
     * @dataProvider existsQueries
     */
    public function testExistsQueries($query, $expected)
    {
        $this->parse($query, $expected);
    }

    /**
     * @param string $query
     * @param string $expected
     *
     * @dataProvider complexQueries
     */
    public function testComplexQueries($query, $expected)
    {
        $this->parse($query, $expected);
    }

    /**
     * @param string $query
     * @param string $message
     *
     * @dataProvider invalidQueries
     */
    public function testInvalidQueries($query, $message)
    {
        $qb        = new QueryBuilder();
        $exception = null;
        try {
            $qb->parse($query);
        } catch (UnrecognizedToken $ex) {
            $exception = $ex;
        }
        $this->assertInstanceOf(UnrecognizedToken::class, $exception);
        $this->assertEquals($message, $exception->getMessage());
    }
}

<?php
namespace query_builder;

use Hoa\Compiler\Llk\Llk;
use Hoa\Compiler\Llk\Parser;
use Hoa\Compiler\Llk\TreeNode;
use Hoa\File\Read;

class QueryBuilder
{
    /**
     * @var Parser
     */
    private $compiler;

    public function __construct()
    {
        $this->compiler = Llk::load(new Read(dirname(__DIR__) . '/resources/grammar.pp'));
    }

    public function parse(string $query): TreeNode
    {
        return $this->compiler->parse($query);
    }
}

<?php

use Hoa\Compiler\Visitor\Dump;
use query_builder\QueryBuilder;

if ($argc <= 1) {
    echo <<<TEXT
Usage:

Run script with PHP7:
   php parse.php query

TEXT;
    return;
}

require 'vendor/autoload.php';

$qb          = new QueryBuilder();
$ast = $qb->parse($argv[1]);

$dump = new Dump();
echo $dump->visit($ast);

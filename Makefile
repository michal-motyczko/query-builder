ENV ?= dev

.PHONY: stan cs md test all
.DEFAULT_GOAL := all

outdated: vendor
	composer show --latest --strict --direct --outdated

security: vendor
	vendor/bin/security-checker security:check

stan: vendor
	vendor/bin/phpstan analyse --no-progress --level 7 src

cs: vendor
	vendor/bin/phpcs --colors --standard=.phpcs.xml src

csfix: vendor
	vendor/bin/phpcbf --colors --standard=.phpcs.xml src

md: vendor
	vendor/bin/phpmd src text .phpmd.xml

composer.lock:
	composer install --prefer-dist --no-progress

vendor: composer.json composer.lock
	composer install --prefer-dist --no-progress

test: vendor
	vendor/bin/phpunit --configuration phpunit.xml --coverage-html test/_data/report --coverage-text=test/_data/report/coverage.txt --log-junit test/_data/report/junit.xml ${PHPUNIT_FLAGS}


all: stan cs md security test
